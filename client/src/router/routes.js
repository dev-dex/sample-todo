const routes = [
  {
    path: "/",
    redirect: {
      path: "welcome",
    },
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "welcome",
        component: () => import("src/pages/LandingPage.vue"),
        meta: { requiresGuest: true },
      },
      {
        path: "auth/sign-in",
        component: () => import("src/pages/SigninPage.vue"),
        meta: { requiresGuest: true },
      },
      {
        path: "auth/sign-up",
        component: () => import("src/pages/SignupPage.vue"),
        meta: { requiresGuest: true },
      },
      {
        path: "dashboard",
        component: () => import("src/pages/DashboardPage.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/ErrorNotFound.vue"),
  },
];

export default routes;
