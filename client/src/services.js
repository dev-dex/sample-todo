// src/services/auth.js

import { ref } from "vue";

const tokenKey = "token";
const accessToken = ref(localStorage.getItem(tokenKey) || "");

export function isLoggedIn() {
  return accessToken.value !== "";
}

export function getAccessToken() {
  return accessToken.value;
}

export function setAccessToken(token) {
  accessToken.value = token;
  localStorage.setItem(tokenKey, token);
}

export function removeAccessToken() {
  accessToken.value = "";
  localStorage.removeItem(tokenKey);
}
