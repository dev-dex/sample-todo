const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const NotFound = require("./middlewares/notfound");
const ErrorHandler = require("./middlewares/errorhandler");
const UserRoutes = require("./routes/user.route");
const AuthRoutes = require("./routes/auth.route");

const cookieParser = require("cookie-parser");

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan("dev"));
app.use(cookieParser());

app.use(
  cors({
    origin: "http://localhost:9000", // Replace with your frontend URL
    credentials: true, // Allow cookies and Authorization headers
  })
);

app.get("/", (req, res) => {
  res.status(200);
  res.json({
    message: "Hello World!",
  });
});

app.use("/api/users", UserRoutes);
app.use("/api/auth", AuthRoutes);

app.use(NotFound);
app.use(ErrorHandler);

module.exports = app;
