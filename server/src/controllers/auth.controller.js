const { User } = require("../models");
const asyncHandler = require("express-async-handler");
const bcrypt = require("bcrypt");
const generateAccessToken = require("../utilities/generate.accesstoken");
const generateRefreshToken = require("../utilities/generate.refreshtoken");
const StoreCookieToken = require("../utilities/storeCookie");
const jwt = require("jsonwebtoken");

const LoginAuth = asyncHandler(async (req, res) => {
  const user = await User.findOne({
    where: {
      user_email: req.body.email,
    },
  });

  if (!user) {
    res.status(401);
    throw new Error("Email and Password does not match");
  }

  const passwordMatch = await bcrypt.compare(
    req.body.password,
    user.user_password
  );

  if (!passwordMatch) {
    res.status(401);
    throw new Error("Email and Password does not match");
  }

  const accessToken = generateAccessToken(user.user_id);
  const refreshToken = generateRefreshToken(user.user_id);

  StoreCookieToken(res, "refreshToken", refreshToken);

  res.status(200).json({
    message: "Login Successfully",
    token: accessToken,
  });
});

const RenewAccessToken = asyncHandler(async (req, res) => {
  const refreshToken = req.cookies.refreshToken;

  if (!refreshToken) {
    res.status(401);
    throw new Error("Missing refresh token in the cookie.");
  }

  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, decode) => {
    if (err) {
      res.status(403);
      throw new Error("The provided token is invalid.");
    }

    const accessToken = generateAccessToken(decode.userId);

    res.json({ new_token: accessToken });
  });
});

module.exports = { LoginAuth, RenewAccessToken };
