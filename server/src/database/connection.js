const { Sequelize } = require("sequelize");
const config = require("./config");

const connection = new Sequelize(config[process.env.NODE_ENV]);

module.exports = connection;
