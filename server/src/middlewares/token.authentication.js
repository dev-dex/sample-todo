const asyncHandler = require("express-async-handler");
const jwt = require("jsonwebtoken");

const RequiredTokenAuth = asyncHandler(async (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];

  if (!token) {
    res.status(401);
    throw new Error("Authorization header with token is required.");
  }

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, decode) => {
    if (err) {
      res.status(403);
      throw new Error("The provided token is invalid.");
    }

    req.userId = decode.userId;

    next();
  });
});

module.exports = RequiredTokenAuth;
