const { DataTypes } = require("sequelize");
const connection = require("../database/connection");

const User = connection.define(
  "User",
  {
    user_id: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4,
    },
    user_name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    user_email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    user_password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    freezeTableName: true,
    timestamps: false,
    modelName: "User",
  }
);

User.prototype.toJSON = function () {
  return {
    ...this.get(),
    user_password: undefined,
  };
};

module.exports = User;
