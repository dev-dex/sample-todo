const express = require("express");
const { LoginUserValidation } = require("../validators/user.validator");
const {
  LoginAuth,
  RenewAccessToken,
} = require("../controllers/auth.controller");

const router = express.Router();

// SIGN IN CREDENTIALS
router.post("/sign-in", LoginUserValidation, LoginAuth);

// GET NEW ACCESS TOKEN
router.post("/renew", RenewAccessToken);

module.exports = router;
