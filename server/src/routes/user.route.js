const express = require("express");
const { NewUserValidation } = require("../validators/user.validator");
const {
  CreateNewUser,
  GetAuthorizeUser,
} = require("../controllers/user.controller");
const RequiredTokenAuth = require("../middlewares/token.authentication");

const router = express.Router();

// USER PROTECTED ROUTE
router.get("/", RequiredTokenAuth, GetAuthorizeUser);

// CREATE NEW USER
router.post("/", NewUserValidation, CreateNewUser);

module.exports = router;
