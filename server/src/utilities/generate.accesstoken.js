const jwt = require("jsonwebtoken");

const generateAccessToken = (id) => {
  return jwt.sign({ userId: id }, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: process.env.ACCESS_TOKEN_EXPIRED,
  });
};

module.exports = generateAccessToken;
