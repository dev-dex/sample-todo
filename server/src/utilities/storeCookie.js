const StoreCookieToken = (res, name, value) => {
  return res.cookie(name, value, {
    sameSite: "None", // Allow cross-origin requests
    secure: true, // Cookie will only be sent over HTTPS
    partitioned: true,
    httpOnly: true,
    expires: new Date(
      Date.now() + process.env.COOKIE_EXPIRATION_DAY * 24 * 60 * 60 * 1000
    ),
  });
};

module.exports = StoreCookieToken;
