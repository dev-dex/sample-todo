const { body, validationResult } = require("express-validator");

const NewUserValidation = [
  body("name")
    .notEmpty()
    .withMessage("Name is required")
    .isString()
    .withMessage("Name must be a string")
    .isLength({ min: 3, max: 30 })
    .withMessage("Name must be between 3 and 30 characters"),
  body("email")
    .notEmpty()
    .withMessage("Email Address is required")
    .isEmail()
    .withMessage("Invalid Email Address"),
  body("password")
    .notEmpty()
    .withMessage("Password is required")
    .isLength({ min: 3, max: 30 })
    .withMessage("Password must be between 3 and 30 characters"),
  (req, res, next) => {
    const allowFields = ["name", "email", "password"];
    const receiveFields = Object.keys(req.body);

    const extraFields = receiveFields.filter(
      (field) => !allowFields.includes(field)
    );

    if (extraFields.length > 0) {
      res.status(400);
      throw new Error(`Unexpected Fields '${extraFields[0]}'`);
    }

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      // Get the first error message
      const firstError = errors.array()[0].msg;
      res.status(400);
      throw new Error(firstError);
    }

    next();
  },
];

const LoginUserValidation = [
  body("email")
    .notEmpty()
    .withMessage("Email Address is required")
    .isEmail()
    .withMessage("Invalid Email Address"),
  body("password")
    .notEmpty()
    .withMessage("Password is required")
    .isLength({ min: 3, max: 30 })
    .withMessage("Password must be between 3 and 30 characters"),
  (req, res, next) => {
    const allowFields = ["email", "password"];
    const receiveFields = Object.keys(req.body);

    const extraFields = receiveFields.filter(
      (field) => !allowFields.includes(field)
    );

    if (extraFields.length > 0) {
      res.status(400);
      throw new Error(`Unexpected Fields '${extraFields[0]}'`);
    }

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      // Get the first error message
      const firstError = errors.array()[0].msg;
      res.status(400);
      throw new Error(firstError);
    }
    next();
  },
];

module.exports = { NewUserValidation, LoginUserValidation };
